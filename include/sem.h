#ifndef SEM_H
#define SEM_H


#include "create.h"

#define NULL 0

struct sem_s {
    int val;
    struct ctx_s *on_wait;
};

void sem_init(struct sem_s *sem, unsigned int val);
void sem_up(struct sem_s *sem);
void sem_down(struct sem_s *sem);


#endif /* TP3_CREATE_H */
