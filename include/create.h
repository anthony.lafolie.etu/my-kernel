#ifndef TP3_CREATE_H
#define TP3_CREATE_H

#define NULL 0

#include "gdt.h"
#include "idt.h"
#include "ioport.h"

typedef void func_t(void *);

/* RDY: prêt à être lancé
 * ACT: déjà lancé
 * TERM: a fini */
enum state_t {RDY, ACT, TERM,STOP};

struct ctx_s{
    void *esp;
    void *ebp;
    func_t *entrypoint;
    void* stack; /* vers la pile de ce contexte */
    void *args;
    enum state_t state;
    struct ctx_s * next; /* prochaine structure*/
    struct ctx_s * next_on_wait; /*Le prochain non disponible*/
};

extern struct ctx_s * ctx_list; /* liste circulaire des contextes , extern utile car déclarer dans .c*/
extern struct ctx_s * ctx_current; /* context courant */

int create_ctx(struct ctx_s *,int , func_t , void *);
void switch_to_ctx(struct ctx_s *);
void yield();
void* alloc_memory(int);

#endif //TP3_CREATE_H
