#include "sem.h"


void sem_init(struct sem_s *sem, unsigned int val){
    sem->val = val;
    sem->on_wait = NULL;
}

void sem_down(struct sem_s *sem){
    __asm volatile("cli");
    if (sem->val >= 1){
        sem->val = sem->val -1;
    }else{
        ctx_current->state = STOP;
        ctx_current->next_on_wait = sem->on_wait;
        sem->on_wait = ctx_current;
        sem->val = sem->val-1;
        __asm volatile("sti");
        _outb(0xA0, 0x20); /* pré-requis matériel */
        _outb(0x20, 0x20);
        yield();
    }
    __asm volatile("sti");
    _outb(0xA0, 0x20); /* pré-requis matériel */
    _outb(0x20, 0x20);
}

void sem_up(struct sem_s *sem){
    __asm volatile("cli");
    if (sem->val >= 0){
        sem->val = sem->val + 1;
    }else{
        sem->on_wait->state = RDY;
        sem->on_wait = sem->on_wait->next_on_wait;
    }
    __asm volatile("sti");
    _outb(0xA0, 0x20); /* pré-requis matériel */
    _outb(0x20, 0x20);
}
