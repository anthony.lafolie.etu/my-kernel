#include "ioport.h"
#include "gdt.h"
#include "idt.h"
#include "create.h"

void clear_screen();				/* clear screen */
void putc(char aChar);				/* print a single char on screen */
void puts(char *aString);			/* print a string on the screen */
void puthex(int aNumber);			/* print an Hex number on screen */
void controlKeyboard();
char keyboard_map(unsigned char);
void counter(void *args);
void empty_irq(int_regs_t *r) {
}
void yield_irq(int_regs_t *r) {
  yield();
}


struct ctx_s ctx_a,ctx_b;


void test(void *args){
  while(1){
    puthex(10);
  }
}


/* multiboot entry-point with datastructure as arg. */
void main(unsigned int * mboot_info)
{
    /* clear the screen */
    clear_screen();
    puts("Early boot.\n"); 
    puts("\t-> Setting up the GDT... ");
    gdt_init_default();
    puts("done\n");

    puts("\t-> Setting up the IDT... ");
    setup_idt();
    puts("OK\n");
	
    puts("\n\n");

    puts("Hello world ! ");

    create_ctx(&ctx_a,10000,counter,0);
    create_ctx(&ctx_b,10000,controlKeyboard,0);
    idt_setup_handler(0, yield_irq); //appel 18 ms
    //idt_setup_handler(0, empty_irq); 
 	  idt_setup_handler(1, controlKeyboard);
    __asm volatile("sti");
    /* minimal setup done ! */

  
    for(;;){ /* nothing more to do... really nothing ! */
    }
}

/* base address for the video output assume to be set as character oriented by the multiboot */
unsigned char *video_memory = (unsigned char *) 0xB8000;

/* clear screen */
void clear_screen() {
  int i;
  for(i=0;i<80*25;i++) { 			/* for each one of the 80 char by 25 lines */
    video_memory[i*2+1]=0x0F;			/* color is set to black background and white char */
    video_memory[i*2]=(unsigned char)' '; 	/* character shown is the space char */
  }
}

/* print a string on the screen */
void puts(char *aString) {
  char *current_char=aString;
  while(*current_char!=0) {
    putc(*current_char++);
  }
}

/* print an number in hexa */
char *hex_digit="0123456789ABCDEF";
void puthex(int aNumber) {
  int i;
  int started=0;
  for(i=28;i>=0;i-=4) {
    int k=(aNumber>>i)&0xF;
    if(k!=0 || started) {
      putc(hex_digit[k]);
      started=1;
    }
  }
}

/* print a char on the screen */
int cursor_x=0;					/* here is the cursor position on X [0..79] */
int cursor_y=0;					/* here is the cursor position on Y [0..24] */

void setCursor() {
  int cursor_offset = cursor_x+cursor_y*80;
  _outb(0x3d4,14);
  _outb(0x3d5,((cursor_offset>>8)&0xFF));
  _outb(0x3d4,15);
  _outb(0x3d5,(cursor_offset&0xFF));
}

void putc(char c) {
  if(cursor_x>79) {
    cursor_x=0;
    cursor_y++;
  }
  if(cursor_y>24) {
    cursor_y=0;
    clear_screen();
  }
  switch(c) {					/* deal with a special char */
    case '\r': cursor_x=0; break;		/* carriage return */
    case '\n': cursor_x=0; cursor_y++; break; 	/* new ligne */	
    case 0x8 : if(cursor_x>0) cursor_x--; break;/* backspace */
    case 0x9 : cursor_x=(cursor_x+8)&~7; break;	/* tabulation */
						/* or print a simple character */
    default  : 
      video_memory[(cursor_x+80*cursor_y)*2]=c;
      cursor_x++;
      break;
  }
  setCursor();
}

void controlKeyboard(){
  putc(keyboard_map(_inb(0x60)));
}

char keyboard_map(unsigned char c){

  switch(c){
    case 0x10: return 0x51; 
    case 0x30: return 0x42;
    case 0x2E: return 0x43;
    case 0x20: return 0x44;
    case 0x12: return 0x45;
    case 0x21: return 0x46;
    case 0x22: return 0x47;
    case 0x23: return 0x48;
    case 0x17: return 0x49;
    case 0x24: return 0x4A;
    case 0x25: return 0x4B;
    case 0x26: return 0x4C;
    case 0x32: return 0x4D;
    case 0x31: return 0x4E;
    case 0x18: return 0x4F;
    case 0x19: return 0x50;
    case 0x1E: return 0x41; 
    case 0x13: return 0x52;
    case 0x1F: return 0x53;
    case 0x14: return 0x54;
    case 0x16: return 0x55;
    case 0x2F: return 0x56;
    case 0x11: return 0x57;
    case 0x2D: return 0x58;
    case 0x15: return 0x59;
    case 0x2C: return 0x5A;

    case 0x90: return 0x20;
    case 0xB0: return 0x20;
    case 0xAE: return 0x20;
    case 0xA0: return 0x20;
    case 0x92: return 0x20;
    case 0xA1: return 0x20;
    case 0xA2: return 0x20;
    case 0xA3: return 0x20;
    case 0x97: return 0x20;
    case 0xA4: return 0x20;
    case 0xA5: return 0x20;
    case 0xA6: return 0x20;
    case 0xB2: return 0x20;
    case 0xB1: return 0x20;
    case 0x98: return 0x20;
    case 0x99: return 0x20;
    case 0x9E: return 0x20;
    case 0x93: return 0x20;
    case 0x9F: return 0x20;
    case 0x94: return 0x20;
    case 0x96: return 0x20;
    case 0xAF: return 0x20;
    case 0x91: return 0x20;
    case 0xAD: return 0x20;
    case 0x95: return 0x20;
    case 0xAC: return 0x20;
    default: return 0x30;
  }
    return c;
}


void counter(void *args){

  int posx,posy;
  int cpt=0;

  while(1){
      __asm volatile("cli");
      posx=cursor_x;
      posy=cursor_y;

      cursor_x = 70;
      cursor_y = 0;

      puthex(cpt++);

      cursor_x = posx;
      cursor_y = posy;
    __asm volatile("sti");
    _outb(0xA0, 0x20); /* pré-requis matériel */
    _outb(0x20, 0x20);

  }
}


void printchar(void *args){
  while(1){
    /* putc(getc) */
  }
}

/*Pour 3.2 : 
IRQ appelle nouvelle fonction pour affichage caractère
Dans  cette fonction : semaphore + traitement carac + ajout file (tableau)
Print char est la fonction du contexte.

Quand sem est bloqué yiel ne l'appelerai plus 
*/