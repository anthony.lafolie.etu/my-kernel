#include "create.h"


char *memory_tab[100000]; /* 10 contextes */
void *memory_ptr = memory_tab;
struct ctx_s *ctx_list = NULL;
struct ctx_s *ctx_current = NULL;

int create_ctx(struct ctx_s *context,int stack_size, func_t f, void *args) {
    context->entrypoint = f;
    context->args = args;
    context->state = RDY;
    context->stack = alloc_memory(stack_size);/* fonction alloc */
    context->esp = context->stack;/* context->stack + sizeof(context->stack) - 4; */
    context->ebp = context->stack;/* context->esp;  Je ne sais pas quoi mettre */

    __asm volatile("cli"); /* empeche interruption pour le bon deroulement du chainage */

    if(ctx_list == NULL){ /* C'est notre premier context */
        ctx_list->next = context;
        ctx_list = context;
    }else{ /* on insère le ctx dans la liste*/
        context->next = ctx_list->next ;
        ctx_list->next = context;
    }

    __asm volatile("sti"); /* remettre interruption */
    _outb(0xA0, 0x20); /* pré-requis matériel */
    _outb(0x20, 0x20);
    return 0;
}

void* alloc_memory(int stack_size){
    void *ptr = memory_ptr;
    memory_ptr+= stack_size;
    return memory_ptr;
}

void yield(){
    if (ctx_current == NULL){
        switch_to_ctx(ctx_current->next);
    }
};


void switch_to_ctx(struct ctx_s *ctx){
    if(ctx_current != NULL){ /* On peut switch*/
        asm("mov %%esp, %0" "\n\t"
            "mov %%ebp, %1"
        : "=r" (ctx_current->esp), "=r" (ctx_current->ebp)
        :
        :
        );
    }/*else{
        unsigned long esp, ebp;
        asm("mov %%esp, %0"  "\n\t"
            "mov %%ebp, %1"
        : "=r" (ctx_current->esp), "=r" (ctx_current->ebp)
        :
        :
        );

    }*/
    ctx_current = ctx;
    asm("mov %0, %%esp" "\n\t"
        "mov %1, %%ebp"
    :
    : "r" (ctx->esp), "r" (ctx->ebp)
    :
    );

    if(ctx_current->state == RDY){ /* Disponible */
        ctx_current->state = ACT;
        ctx_current->entrypoint(ctx_current->args);
        ctx_current->state = TERM;
        __asm volatile("cli");
        /* déchainage */
        struct ctx_s *previous = ctx_list; /* on se met au début de la liste circulaire */
        if (ctx_current->next == ctx_current){
                ctx_current = NULL;
        }else{
            while (previous->next != ctx_current){
                previous = previous->next;
            }
        }
        previous->next = ctx_current->next;


        __asm volatile("sti");
        _outb(0xA0, 0x20); /* pré-requis matériel */
        _outb(0x20, 0x20);
        yield();
    }


    return;
}


